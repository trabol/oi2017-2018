#include <iostream>
 #include<bits/stdc++.h>
using namespace std;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int t;
    cin>>t;
    while(--t>=0){
        int n,m,k;
        cin>>n>>m>>k;
        int komp[n+1];
        bool czyByliKomp[n+1];
        int aut[m+1];
        bool czyByliAut[m+1];
        for(int i=0;i<=n;i++)komp[i]=-1,czyByliKomp[i]=false;;
        for(int i=0;i<=m;i++)aut[i]=-1,czyByliAut[i]=false;
        bool isOk=true;
        for(int i=0;i<k;i++){
            int a,b,c,p;
            cin>>a>>b>>c>>p;
            czyByliKomp[a]=true;
            if(komp[a]==-1)komp[a]=p;
            else if(komp[a]!=p){isOk=false;
            }
            for(int j=b;j<=c;j++){
                czyByliAut[j]=false;
                if(aut[j]==-1)aut[j]=p;
                else if(aut[j]!=p){
               isOk=false;
            }
            }
        }
        bool isNOtmarkedkomp=false;
        for(int i=0;i<=n;i++){
            if(!czyByliKomp[i]){
                if(!(komp[i]==-1||komp[i]==0))isNOtmarkedkomp=true,break;
            }
        }
        bool isNotmarkedayt=false;
        for(int i=0;i<=m;i++){
            if(!czyByliAut[i]){
                if((!aut[i]==-1||aut[i]==0))isNotmarkedayt=true,break;
            }
        }
        if(isOk)cout<<"TAK\n";
        else cout<<"NIE\n";
    }

}
