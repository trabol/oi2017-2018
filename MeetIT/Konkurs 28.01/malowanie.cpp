#include <iostream>
 #include<bits/stdc++.h>
using namespace std;

struct Kwadrat{
    int x1,y1,x2,y2;
};


int n;
Kwadrat kwadraty[500000];
Kwadrat prefix[500000];
Kwadrat sufix[500000];

long long surface(Kwadrat k){
    return(k.x2-k.x1)*(k.y2-k.y1);
}

void read(){
    for(int i=0;i<n;i++){
        Kwadrat k1;
        cin>>k1.x1>>k1.y1>>k1.x2>>k1.y2;
        kwadraty[i]=k1;
    }
}

void licz_prefix(){
    prefix[0]=kwadraty[0];
    for(int i=1;i<n;i++){
        Kwadrat k=prefix[i-1];
        k.x1=max(k.x1,kwadraty[i].x1);
        k.y1=max(k.y1,kwadraty[i].y1);
        k.x2=min(k.x2,kwadraty[i].x2);
        k.y2=min(k.y2,kwadraty[i].y2);
        prefix[i]=k;
    }
}

void licz_sufix(){
    sufix[n-1]=kwadraty[n-1];
    for(int i=n-2;i>=0;i--){
        Kwadrat k=sufix[i+1];
        k.x1=max(k.x1,kwadraty[i].x1);
        k.y1=max(k.y1,kwadraty[i].y1);
        k.x2=min(k.x2,kwadraty[i].x2);
        k.y2=min(k.y2,kwadraty[i].y2);
        sufix[i]=k;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin>>n;
    read();
    licz_prefix();
    licz_sufix();

    long long totalSurface=0;
    Kwadrat now;
    for(int a=0;a<n;a++){
            if(a==0){
                now=sufix[1];
            }else if(a==n-1){
                now=prefix[n-2];
            }else{
                now=prefix[a-1];
                now.x1=max(now.x1,sufix[a+1].x1);
                now.y1=max(now.y1,sufix[a+1].y1);
                now.x2=min(now.x2,sufix[a+1].x2);
                now.y2=min(now.y2,sufix[a+1].y2);
            }
        totalSurface+=surface(now);
    }

    totalSurface-=(n-1)*surface(sufix[0]);
    cout<<totalSurface;

}
