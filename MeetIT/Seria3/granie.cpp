#include <iostream>
#include <algorithm>
using namespace std;

int compare(const void * a, const void * b){
 const int arg1 = *static_cast<const int*>(a);
 const int arg2 = *static_cast<const int*>(b);
     if( arg1==arg2){
        return 0;
     }else if(arg1>arg2){
        return 1;
     }else{
        return -1;
     }
 }

int main(){
    //ios_base::sync_with_stdio(false);
    //cin.tie(0);
    int n;
    cin>>n;
    long long ciag[n+1];
    ciag[0]=0;
    for(int i=1;i<=n;i++){
        cin>>ciag[i];
        ciag[i]+=ciag[i-1];
    }
    qsort(ciag,n,sizeof(long long),compare);
    //for(int i=0;i<n;i++)cout<<ciag[i]<<" ";
    //cout<<endl;
    int p;
    cin>>p;
    long long req[p];
    for(int i=0;i<p;i++){
        cin>>req[i];
    }
    for(int i=0;i<p;i++){
        int l=0,pr=n;
        while(l<pr){
                //cout<<"w "<<l<<" "<<p<<endl;
                int s=(l+pr)/2;
                if(ciag[s]<req[i]){
                    l=s+1;
                }else{
                    pr=s;
                }
        }
        //cout<<"ciag"<<ciag[p]<<" "<<req[i]<<" ";
        if(ciag[pr]==req[i]){
            cout<<"TAK"<<endl;
        }else{
            cout<<"NIE"<<endl;
        }
    }

}
