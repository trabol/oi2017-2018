#include <iostream>
#include <algorithm>
using namespace std;

int compare(const void * a, const void * b){
 const int arg1 = *static_cast<const int*>(a);
 const int arg2 = *static_cast<const int*>(b);
     if( arg1==arg2){
        return 0;
     }else if(arg1>arg2){
        return 1;
     }else{
        return -1;
     }
 }

int main(){
    //ios_base::sync_with_stdio(false);
    //cin.tie(0);
    static bool liczby[1000001];
    for(int i=0;i<1000001;i++)liczby[i]=false;
    int n;
    cin>>n;
    long long ciag[n];
    cin>>ciag[0];
    liczby[ciag[0]]=true;
    for(int i=1;i<n;i++){
        cin>>ciag[i];
        ciag[i]+=ciag[i-1];
        liczby[ciag[i]]=true;
    }
    //for(int i=0;i<n;i++)cout<<ciag[i]<<" ";
    //cout<<endl;
    int p;
    cin>>p;
    long long req[p];
    for(int i=0;i<p;i++){
        cin>>req[i];
    }
    for(int i=0;i<p;i++){

        if(liczby[req[i]]){
            cout<<"TAK"<<endl;
        }else{
            cout<<"NIE"<<endl;
        }
    }

}
