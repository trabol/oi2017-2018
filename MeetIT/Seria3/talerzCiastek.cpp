#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
   int n,m;
   cin>>n>>m;
   int minimum=min(m,n);
    bool tab[n][m];
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            char a;
            cin>>a;
            tab[i][j]=(a=='#');
        }
    }

    for(int i=0;i<n;i++){
        int start=-1,ending=-1;
        for(int j=0;j<m;j++){
            if(tab[i][j]){
                if(start>-1){
                    ending=j;
                }else{
                    start=j;
                    ending=j;
                }
            }else{
                if(start>-1){
                    minimum=min(minimum,ending-start+1);
                    start=-1;
                    ending=-1;

                }
            }
        }
    }
    for(int j=0;j<m;j++){
        int start=-1,ending=-1;
        for(int i=0;i<n;i++){
            if(tab[i][j]){
                if(start>-1){
                    ending=i;
                }else{
                    start=i;
                    ending=i;
                }
            }else{
                if(start>-1){
                    minimum=min(minimum,ending-start+1);
                    start=-1;
                    ending=-1;

                }
            }
        }
    }
    cout<<minimum;

}
