#include <iostream>
#include<bits/stdc++.h>
using namespace std;



struct Tygrys{
    int r;
    int minR;
};

Tygrys conTygrys(int r,int minR){
    Tygrys tygrys;
    tygrys.r=r;
    tygrys.minR=minR;
    return tygrys;
}

struct classcompByMinR {
  bool operator() (const Tygrys& lhs, const Tygrys& rhs) const
  {
      if(lhs.minR<rhs.minR){
        return true;
      }
      return false;
  }
};

struct classcompByR {
  bool operator() (const Tygrys& lhs, const Tygrys& rhs) const
  {
      if(lhs.r<rhs.r){
        return true;
      }
      return false;
  }
};

int n;
multiset<Tygrys,classcompByR> tygrysyByR;
multiset<Tygrys,classcompByMinR> tygrysyByMinR;

void read(){
    cin>>n;
    for(int i=0;i<n;i++){
        int r,k;
        cin>>r>>k;
        Tygrys tyg=conTygrys(r,r/k);
        tygrysyByR.insert(tyg);
        tygrysyByMinR.insert(tyg);
    }
}

void coutMultisets(){
    for(multiset<Tygrys,classcompByR>::iterator it=tygrysyByR.begin();it!= tygrysyByR.end();it++){
        cout<<it->r<<" "<<it->minR<<endl;
    }
    cout<<"---------------------------------------------------------------------------------"<<endl;
    for(multiset<Tygrys,classcompByMinR>::iterator it=tygrysyByMinR.begin();it!= tygrysyByMinR.end();it++){
        cout<<it->r<<" "<<it->minR<<endl;
    }
}
int main(){
     ios_base::sync_with_stdio(false);
   cin.tie(0);
    read();
    coutMultisets();
    int ile=0;
    for(multiset<Tygrys,classcompByR>::iterator it=tygrysyByR.begin();it!= tygrysyByR.end()&&!tygrysyByR.empty();){
            cout<<"take R"<<endl;
        Tygrys curTygrys=*it;
        it++;
        tygrysyByR.erase(conTygrys(curTygrys.r,curTygrys.minR));
        tygrysyByMinR.erase(conTygrys(curTygrys.r,curTygrys.minR));
        cout<<"erased R "<<curTygrys.r<<" "<<curTygrys.minR<<endl;
        coutMultisets();
        for(multiset<Tygrys,classcompByMinR>::iterator it2=tygrysyByMinR.begin();it2!= tygrysyByMinR.end()&&!tygrysyByMinR.empty()&&it2->minR<=curTygrys.r;){
                cout<<"take minR"<<endl;
        Tygrys curTygrys2=*it2;
        it2++;
        tygrysyByMinR.erase(conTygrys(curTygrys2.r,curTygrys2.minR));
        tygrysyByR.erase(conTygrys(curTygrys2.r,curTygrys2.minR));
        cout<<"erased minR"<<curTygrys2.r<<" "<<curTygrys2.minR<<endl;
        coutMultisets();
        }
        ile++;

    }
    cout<<ile;
}
