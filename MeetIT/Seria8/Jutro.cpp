#include <iostream>
#include<bits/stdc++.h>
using namespace std;



struct Task{
    int length;
    int deadline;
};

struct classcomp {
  bool operator() (const Task& lhs, const Task& rhs) const
  {return lhs.deadline<rhs.deadline;}
};

int n;

void coutTasks(multiset<Task,classcomp> tasks){
    for(multiset<Task,classcomp>::iterator i=tasks.begin();i!=tasks.end();i++){
        cout<<"[ "<<i->length<<","<<i->deadline<<"] ";
    }
    cout<<endl;
}





int main(){
       ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin>>n;
    multiset<Task,classcomp> tasks ;
    for(int i=0;i<n;i++){
        Task task;
        cin>>task.length>>task.deadline;
        tasks.insert(task);
    }

    //coutTasks(tasks);

    int minGap=2000000000;
    int sumLength=0;
    for(multiset<Task,classcomp>::iterator i=tasks.begin();i!=tasks.end();i++){
        sumLength+=i->length;
        minGap=min(minGap,i->deadline-sumLength);
    }
    cout<<minGap;
}
