#include <iostream>

using namespace std;
const int MAX_N=2000;
int MAX_SKIB=2*MAX_N;
int wynikOST=MAX_SKIB;
int tab[MAX_N][MAX_N];

int prawo[MAX_N+1][MAX_N+1];
int dol[MAX_N+1][MAX_N+1];
int m,n,k;

void wczytaj(){
    cin>>k>>m>>n;
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            cin>>tab[i][j];
        }
    }
}
void liczSumy(){
    for (int i = 0; i <= n; ++i)
    dol[i][m] = prawo[i][m] = 0;

  for (int j = 0; j <= m; ++j)
    dol[n][j] = prawo[n][j] = 0;

  for (int i = n - 1; i >= 0; --i)
    for (int j = m - 1; j >= 0; --j)
    {
      dol[i][j] = tab[i][j] + dol[i+1][j];
      prawo[i][j] = tab[i][j] + prawo[i][j+1];
    }
}
void pisz(){
    for (int i = 0; i < n; ++i){
    for (int j = 0; j < m; ++j){
      cout<<dol[i][j]<<" ";

    }
    cout<<endl;
    }
    cout<<"UUUUUUUUUUUU"<<endl;

    for (int i = 0; i < n; ++i){
    for (int j = 0; j < m; ++j){
      cout<<prawo[i][j]<<" ";

    }
    cout<<endl;
    }
    cout<<"--------------"<<endl;;
}

bool moznaOracPoziom(int lewe,int prawe){
    int i=0,j=0,i2=n-1,j2=m-1;
    while(i<=i2){
        if(prawo[i][j]-prawo[i][j2+1]<=k){
            i++;
        }else if(prawo[i2][j]-prawo[i2][j2+1]<=k){
            i2--;
        }else if(dol[i][j]-dol[i2+1][j]<=k&&lewe){
            j++;
            lewe--;
        }else if(dol[i][j2]-dol[i2+1][j2]<=k&&prawe){
            j2--;
            prawe--;
        }else{
            return false;
        }
    }
    return true;
}
 int liczPoziomo(){
    int wynik = n + m;
  liczSumy();
  for (int i = 0; i < m; ++i)
  {
    int prawe_ciecia = i;
    int wa, wb, ka, kb; /*aktualne wymiary pola: gorny wiersz, dolny wiersz,  *
                         *lewa kolumna, prawa kolumna                         */
    wa = ka = 0;
    wb = n - 1;
    kb = m - 1;
    while (wa <= wb && ka <= kb)
    {
      if (prawo[wa][ka] - prawo[wa][kb+1] <= k)
      {
        /*mozna wykonac ciecie gorne  */
        if (wa++ == wb)
          wynik = min(wynik, n + m - (kb - ka + 1));
      } else
        if (prawo[wb][ka] - prawo[wb][kb+1] <= k)
        {
          /*mozna wykonac ciecie dolne*/
          if (wa == wb--)
            wynik = min(wynik, n + m - (kb - ka + 1));
        } else
          if (dol[wa][kb] - dol[wb+1][kb] <= k && prawe_ciecia)
          {
            /*mozna wykonac ciecie prawe*/
            kb--;
            prawe_ciecia--;
          }
          else
            if (dol[wa][ka] - dol[wb+1][ka] <= k)
              ka++; /*ciecie lewe*/
            else
              break; /*nie da sie wykonac zadnego ciecia*/

    }
  }
  return wynik;
}

void transponuj()
{
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      dol[i][j] = tab[i][j];

  swap(n, m);
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      tab[i][j] = dol[j][i];
}
int main(){
    ios_base::sync_with_stdio(false);
   cin.tie(0);
    wczytaj();
    liczSumy();
    wynikOST=liczPoziomo();
    transponuj();
    liczSumy();
    wynikOST=min(wynikOST,liczPoziomo());
    cout<<wynikOST;
}
