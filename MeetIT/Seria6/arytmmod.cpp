#include <iostream>

using namespace std;

long long M=1000000000+ 696969;

long long pow(long long base, long long power){
    if(power==0)return 1;
    if(power%2==0){
        long long result=pow(base%M,power/2)%M;
        return (result%M*result%M)%M;
    }else return (base%M*pow(base%M,power-1))%M;}

int main(){

    long long a;
    long long b;
    long long c;
    long long d;
    cin>>a>>b>>c>>d;
    a=a%M;
    b=b%M;
    c=c%M;
    d=d%M;

    if(a<0){
        cout<<"a";
    }
    if(b<0){
        cout<<"b";
    }
    if(c<0){
        cout<<"c";
    }
    if(d<0){
        cout<<"d";
    }
    long long ab=(a%M*b%M)%M;
    if(ab<0){
        cout<<" AB "<<a<<"QQ"<<b<<"ZZ";
    }
    long long cd = (c%M*d%M)%M;
    if(ab<0){
        cout<<"CD"<<c<<"QQ"<<d<<"ZZ";
    }
    //ab/cd == ab*cd^(-1)
    long long cddominus1=pow(cd%M,M-2)%M;
    if(ab<0){
        cout<<"CDDOMINUS1";
    }
    long long wynik=(ab%M*cddominus1%M)%M;
    if(wynik<0){
        cout<<"WYNIK";
    }
    cout<<wynik<<endl;;
    //cout<<(a%M*b%M)%M*pow(c%M*d%M,M-2)%M<<endl;
}
