#include <iostream>
#include <algorithm>
using namespace std;

bool isVovel(char a){
    return a=='A'||a=='E'||a=='I'||a=='O'||a=='U'||a=='Y'||a=='a'||a=='e'||a=='i'||a=='o'||a=='u'||a=='y';
}

int main(){
    //ios_base::sync_with_stdio(false);
    //cin.tie(0);
    int n;
    cin>>n;
    string naszyjniki[n];
    for(int i=0;i<n;i++)cin>>naszyjniki[i];

    for(int i=0;i<n;i++){
        //male duze

        int maxL=naszyjniki[i].length();
        int start=0;
        while(start<maxL&&isupper(naszyjniki[i][0])==isupper(naszyjniki[i][start])&&isVovel(naszyjniki[i][0])==isVovel(naszyjniki[i][start]))start++;

        while(maxL-1>start&&isupper(naszyjniki[i][0])==isupper(naszyjniki[i][maxL-1])&&isVovel(naszyjniki[i][0])==isVovel(naszyjniki[i][maxL-1]))maxL--;
        int minCiag=2000000000;
        bool isMinDuzy=false;
        bool isMinJasny=false;
        int maxCiag=0;
        bool isMaxJasny=false;
        bool isMaxDuzy=false;
        cout<<start<<" "<<maxL<<endl;
        if(start==maxL){
            minCiag=maxCiag=start;
        }else{
            minCiag=maxCiag=start+naszyjniki[i].length()-maxL;
        }
        isMinDuzy=isMaxDuzy=isupper(naszyjniki[i][0]);
        isMinJasny=isMaxJasny=isVovel(naszyjniki[i][0]);
        int j=start,k=j+1;
        while(j<maxL){
            bool isCurDuzy=isupper(naszyjniki[i][j]);
            bool isCurJasny=isVovel(naszyjniki[i][j]);

            while(k<naszyjniki[i].length()&&isCurDuzy == isupper(naszyjniki[i][k])&&isCurJasny==isVovel(naszyjniki[i][k]))k++;
            if(minCiag>=k-j){
               if(minCiag>k-j){
                minCiag=k-j;
                isMinDuzy=isCurDuzy;
                isMinJasny=isCurJasny;
               }else{
                    if(isCurDuzy==isMinDuzy){
                        isMinJasny=(isCurJasny||isMinJasny);
                    }else if(isCurDuzy&&!isMinDuzy){
                        isMinDuzy=isCurDuzy;
                        isMinJasny=isCurJasny;
                    }
               }

            }
            if(maxCiag<=k-j){
                if(maxCiag<k-j){
                maxCiag=k-j;
                isMaxDuzy=isCurDuzy;
                isMaxJasny=isCurJasny;
               }else{
                    if(isCurDuzy==isMaxDuzy){
                        isMaxJasny=(isCurJasny||isMaxJasny);
                    }else if(isCurDuzy&&!isMaxDuzy){
                        isMaxDuzy=isCurDuzy;
                        isMaxJasny=isCurJasny;
                    }
               }
            }
            j=k;
            k++;
        }
        cout<<maxCiag<<" ";
        if(isMaxDuzy){
            if(isMaxJasny){
                cout<<"JASNE";
            }else{
                cout<<"CIEMNE";
            }
        }else{
            if(isMaxJasny){
                cout<<"jasne";
            }else{
                cout<<"ciemne";
            }
        }
        cout<<endl<<minCiag<<"  ";
        if(isMinDuzy){
            if(isMinJasny){
                cout<<"JASNE";
            }else{
                cout<<"CIEMNE";
            }
        }else{
            if(isMinJasny){
                cout<<"jasne";
            }else{
                cout<<"ciemne";
            }
        }
        cout<<endl;
    }

}
