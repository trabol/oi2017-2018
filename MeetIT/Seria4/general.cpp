#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
   int n,k;

   cin>>n>>k;
   int in[n];
   for(int i=0;i<n;i++)cin>>in[i];

   int i=0,j=1;
   while(i<n){
        while(j<n&&in[i]+k>=in[j]){
            j++;
        }
        in[i]=j;
        i++;
   }

   for(int i=0;i<n;i++)cout<<in[i]<<" ";

}
