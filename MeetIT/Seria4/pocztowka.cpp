#include <iostream>
#include <algorithm>
using namespace std;


int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    long long n,k;
    cin>>n>>k;
    long long tab[n];
    for(int i=0;i<n;i++)cin>>tab[i];

    long long suma=0;
    long long ileWiekszych=0;
    long long ilePopszednikow=0;
    long long start=0;
    long long pozycjaPoprzedniej;

    while(tab[start]<k)start++;
    if(start<n&&tab[start]>=k){
    ilePopszednikow+=start;
     pozycjaPoprzedniej=start++;

    ileWiekszych++;
    suma+=ileWiekszych+ilePopszednikow;
    }

    //cout<<"start "<<start<<endl;
    for(int i=start;i<n;i++){
            //cout<<i<<" "<<suma<<endl;
        if(tab[i]>=k){
            ileWiekszych++;
            ilePopszednikow+=i-pozycjaPoprzedniej-1;
            pozycjaPoprzedniej=i;
        }
    suma+=ilePopszednikow+ileWiekszych;
    }

    cout<<suma;
}
