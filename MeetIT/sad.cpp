#include <iostream>
#include <math.h>
using namespace std;

int main(){
    int n;
    cin>>n;
    int tab[n][2];
    for(int i=0;i<n;i++){
        cin>>tab[i][0]>>tab[i][1];
    }
    int maxX=0,minX=2000000,maxY=0,minY=2000000;
    for(int i=0;i<n;i++){
        maxX=max(maxX,tab[i][0]);
        minX=min(minX,tab[i][0]);
        maxY=max(maxY,tab[i][1]);
        minY=min(minY,tab[i][1]);
    }
    cout<<2*(maxX-minX)+2*(maxY-minY);
}
