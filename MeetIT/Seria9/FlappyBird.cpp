#include <iostream>
#include<bits/stdc++.h>
using namespace std;

int n,X;
int tab[500000][3];

void prepare(){
    int positionX=0;
  int positionY=0;
  int x=tab[0][0]-positionX;//dx
    int dY=positionY-x;//Y dol
    int uY=positionY+x;
    if(!((tab[0][1]+dY)%2==0))tab[0][1]++;
    if(!((tab[0][2]+uY)%2==0))tab[0][2]--;
    tab[0][1]=max(dY,tab[0][1]);
    tab[0][2]=min(uY,tab[0][2]);
}

bool process(int i,int j){
    int x=tab[j][0]-tab[i][0];
    int dY=tab[i][1]-x;
    int uY=tab[i][2]+x;
    if(!((tab[j][1]+dY)%2==0)){tab[j][1]++;}
    if(!((tab[j][2]+uY)%2==0))tab[j][2]--;
    tab[j][1]=max(dY,tab[j][1]);
    tab[j][2]=min(uY,tab[j][2]);

    return tab[j][1]<=tab[j][2];
}

int countTaps(){
    int wynik=0;
    for(int i=n-1;i>0;i--){
        int x=tab[i][0]-tab[i-1][0];
        int h=tab[i][1]-tab[i-1][1];
        wynik+=(x+h)/2;
    }
    int x=tab[0][0];
    int h=tab[0][1];
    wynik+=(x+h)/2;
    return wynik;
}

void printAll(){
    for(int i=0;i<n;i++){
    cout<<tab[i][0]<<" "<<tab[i][1]<<" "<<tab[i][2]<<endl;
  }
}

int main(){
    ios_base::sync_with_stdio(false);
   cin.tie(0);

  cin>>n>>X;

  for(int i=0;i<n;i++){
    cin>>tab[i][0]>>tab[i][1]>>tab[i][2];
    tab[i][1]++;
    tab[i][2]--;
  }
  //cout<<endl;
  prepare();
  if(tab[0][2]<tab[0][1]){
    cout<<"NIE";
    //cout<<tab[0][1]<<" "<<tab[0][2]<<endl;
    return 0;
  }

  for(int i=0;i<n-1;i++){
        //cout<<"i"<<i<<endl;
    if(!process(i,i+1)){
        cout<<"NIE"<<endl;
        //cout<<i<<endl;
        //printAll();
        return 0;
    }

  }
  //printAll();
  cout<<countTaps();


}
