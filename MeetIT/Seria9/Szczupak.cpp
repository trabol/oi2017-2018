#include <iostream>
#include<bits/stdc++.h>

using namespace std;

int n;
string word;

int analyzeWord(int p,int k, int l){
    bool wasFound=false;
    while(p<n&&!wasFound){
        int k2=k;
        while(p<k2-1&&word[p]!=word[k2]){
            k2-=1;
        }
        if(word[p]!=word[k2]){
            p++;
        }else{
            k=k2;
            if(p==k){
                return l+1;
            }else if(p==k-1){
            return l+2;}
        else if(p==k-2){
            return l+3;
        }else{
            l+=2;
            wasFound=true;
        }
        }
    }
    if(!wasFound){
        return l+1;}
    int maxL=l;
    for(int i=p+1;i<k;i++){
        maxL=max(maxL,analyzeWord(i,k-1,l));
    }
    return maxL;
}

int analyzeWord2(int p){
    int k=n-1,l=0;
    while(p<n&&p<k){
        l++;
        while(p<k){
            if(word[p]==word[k]){
                l++;
                break;
            }else{
                k--;
            }
        }

    }
    return l;
}

int main(){
   ios_base::sync_with_stdio(false);
   cin.tie(0);
   cin>>n;
   cin>>word;
   int wynik=0;
   for(int i=0;i<n;i++){
        int a=analyzeWord(i,n-1,0);
        if(wynik<analyzeWord(i,n-1,0)){
            wynik=a;
        }
   }
   cout<<wynik;
}
