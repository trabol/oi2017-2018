// Wyszukiwanie cykli i �cie�ek Hamiltona
// Data: 10.02.2014
// (C)2014 mgr Jerzy Wa�aszek
//---------------------------------------

#include <iostream>
#include <bits/stdc++.h>

using namespace std;


struct Head{
    long long number,u,z;
    vector<int> grows;
};

bool compare(const Head& lhs,const Head& rhs){
    return lhs.z<rhs.z;
}

int n;
vector<Head> tab;
long long koszt[200000];

void read(){
    cin>>n;
    string s;
    getline(cin,s);
    for(int i=1;i<=n;i++){
        string line;
        getline(cin,line);
        istringstream iss(line);
        Head head;
        head.number=i;
        iss>>head.u;
        iss>>head.z;
        int inti;
        while(iss>>inti){
            head.grows.push_back(inti);
        }
        tab.push_back(head);
    }
}

void write(){
    cout<<endl;
    for(int i=1;i<=n;i++){
        Head head=tab[i];
        cout<<head.u<<" "<<head.z<<" ";
        for(int j=0;j<head.grows.size();j++){
            cout<<head.grows[j]<<" ";
        }
        cout<<endl;
    }
}

void prepare(){
    Head emptyHead;
    emptyHead.u=-1;
    emptyHead.z=-1;
    tab.push_back(emptyHead);
for(int i=0;i<n;i++)koszt[i]=-1;

}

void sortByZ(){
    sort(tab.begin(),tab.end(),compare);
}



int main(){
    tab.reserve(200000);
    read();
    prepare();
    sortByZ();
    write();
    for(int i=1;i<=n;i++){
        if(koszt[i]==-1){
            koszt[i]=tab[i].z;
        }
        for(int j=i+1;j<n;j++){
                bool hasAll=true;
                long long koszcik=0;
            for(int g=0;g<tab[j].grows.size();g++){
                if(koszt[tab[j].grows[g]]==-1){
                    hasAll=false;break;
                }
                koszcik+=koszt[tab[j].grows[g]];
            }
            if(!hasAll)continue;
            koszt[j]=min(tab[j].z,tab[j].u+koszcik);
        }
    }

    //cout<<koszt[1]<<endl;
    for(int i=1;i<=n;i++){
            //cout<<tab[i].number<<endl;
        //if(tab[i].number==1)cout<<koszt[i]<<endl;
        cout<<koszt[i]<<endl;
    }
}
