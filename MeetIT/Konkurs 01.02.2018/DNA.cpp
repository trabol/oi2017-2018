// Wyszukiwanie cykli i �cie�ek Hamiltona
// Data: 10.02.2014
// (C)2014 mgr Jerzy Wa�aszek
//---------------------------------------

#include <iostream>
#include <bits/stdc++.h>

using namespace std;
int n;
string in;
string out;

int lcs(){
    int dp[n+1][n+1];
    for(int i=0;i<=n;i++)dp[i][0]=0;
    for(int j=0;j<=n;j++)dp[0][j]=0;

    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            int m=max(dp[i+1][j],dp[i][j+1]);
            if(in[i]==out[j]){
                dp[i+1][j+1]=dp[i][j]+1;
            }else{
                dp[i+1][j+1]=max(dp[i+1][j],dp[i][j+1]);
            }
        }
    }
    return dp[n][n];
}


int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin>>n;
    cin>>in;
    for(int i=0;i<n;i++){
        if(in[i]=='A'){
            out[i]='C';
        }else if(in[i]=='C'){
            out[i]='A';
        }else if(in[i]=='G'){
            out[i]='T';
        }else{
            out[i]='G';
        }
    }
    cout<<lcs()<<endl;
    for(int i=0;i<n;i++){
        cout<<out[i];
    }
}
