// Wyszukiwanie cykli i �cie�ek Hamiltona
// Data: 10.02.2014
// (C)2014 mgr Jerzy Wa�aszek
//---------------------------------------

#include <iostream>
#include <bits/stdc++.h>

using namespace std;
int n;
string in;
string out;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin>>n;
    cin>>in;
    int ileA=0,ileC=0,ileG=0,ileT=0;
    for(int i=0;i<n;i++){
        if(in[i]=='A'){
            ileA++;
        }else if(in[i]=='C'){
            ileC++;
        }else if(in[i]=='G'){
            ileG++;
        }else{
            ileT++;
        }
    }
    int lcsd=min(ileA,min(ileC,min(ileT,ileG)));
    char ch;
    if(ileA==lcsd){
        lcsd=ileA;
        ch='A';
    }else if(ileC==lcsd){
        lcsd=ileC;
        ch='C';
    }else if(ileG==lcsd){
        lcsd=ileG;
        ch='G';
    }else{
        lcsd=ileT;
        ch='T';
    }
    for(int i=0;i<n;i++)out+=ch;
    cout<<lcsd<<endl;
    for(int i=0;i<n;i++){
        cout<<out[i];
    }
}
