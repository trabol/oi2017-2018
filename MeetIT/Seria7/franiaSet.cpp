#include <iostream>
 #include<bits/stdc++.h>
using namespace std;


int compare(const void * a, const void * b){
 const int arg1 = *static_cast<const int*>(a);
 const int arg2 = *static_cast<const int*>(b);
     if( arg1==arg2){
        return 0;
     }else if(arg1<arg2){
        return 1;
     }else{
        return -1;
     }
 }

int main(){
    int n,k;
    cin>>n>>k;
    if(k<n){
        cout<<"NIE";
        return 0;
    }
    int P[n];
    multiset<int> K;
    bool isKolor[k];

    for(int i=0;i<n;i++)isKolor[i]=true;

    for(int i=0;i<n;i++){
            cin>>P[i];
    }
    qsort(P,n,sizeof(int),compare);
    for(int i=0;i<k;i++){
            int a;
            cin>>a;
            K.insert(a);
    }
    int ileK=0;
    for(int i=0;i<n;i++){
        multiset<int>::iterator iter1=K.lower_bound(P[i]*5);
        if(iter1!=K.end()){
            ileK++;
            K.erase(iter1);
            continue;
        }

        multiset<int>::iterator iter2=K.lower_bound(P[i]*3);
        if(iter2==K.end()){
            cout<<"NIE";
            return 0;
        }
        K.erase(iter2);
        multiset<int>::iterator iter3=K.lower_bound(P[i]*2);
        if(iter3==K.end()){
            cout<<"NIE";
            return 0;
        }
        K.erase(iter3);
        ileK+=2;

    }
    cout<<ileK;



}
