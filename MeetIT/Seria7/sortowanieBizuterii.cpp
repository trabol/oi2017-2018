#include <iostream>
# include <algorithm>
using namespace std;



int compare(const void * a, const void * b){
 const string arg1 = *static_cast<const string*>(a);
 const string arg2 = *static_cast<const string*>(b);
     if( arg1.size()==arg2.size()){

        for(int i=0;i<arg1.size();i++){
            if(arg1[i]>arg2[i]){
                return 1;
            }else if(arg1[i]<arg2[i]){
                return -1;
            }
        }
        return 0;
     }else if(arg1.size()>arg2.size()){
        return 1;
     }else{
        return -1;
     }
 }

int main(){
    int n;
    cin>>n;
    string A[n];
    for(int i=0;i<n;i++)cin>>A[i];

    qsort(A,n,sizeof(string),compare);

    for(int i=0;i<n;i++)cout<<A[i]<<endl;;

}
