#include <iostream>

int n, k, s;

static int DP[1000001];
static int m[30];;

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cin >> n >> k >> s;
	for (int i = 0; i<n; i++) {
		cin >> m[i];
	}
	for (int i = 0; i <= s; i++)DP[i]  = -1;
	DP[0]  = 0;
	for (int j = 0; j<n; j++) {
		for (int i = s-m[j]; i >= 0; i--) {
                //cout<<i<<endl;
			if (DP[i]>=0){
                if(DP[i+m[j]]>=0)
                    DP[i + m[j]] = min(DP[i]+1,DP[i+m[j]]);
                else
                    DP[i + m[j]]=DP[i]+1;
			}

		}
	}

	int maxScore = 0;
	/*for (int i = 0; i <= s; i++) {
		cout<<DP[i]<<" ";
	}*/
	for (int i = 0; i <= s; i++) {
		if (DP[i]>=0&&DP[i]<=k)
			maxScore = i;
	}
	cout << maxScore;
	return 0;
}
