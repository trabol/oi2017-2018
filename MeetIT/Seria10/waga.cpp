#include <iostream>

int n, l,p;
int MAXM=62500;
static int DPL1[125001];
static int DPL2[125001];

static int DPP1[125001];
static int DPP2[125001];

int ind(int i){
    return i+=62500;
}

void copyArray(int a1[],int a2[]){
    for(int i=0;i<125001;i++){
        a2[i]=a1[i];
    }
}

void emptyarray(int a1[]){
    for(int i=0;i<125001;i++){
        a1[i]=-1;
    }
}

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cin>>n>>l>>p;
	int lewe[l][n];
	int prawe[p][n];
	//READ

	for(int i=0;i<l;i++){
            cin>>lewe[i][0];
        for(int j=1;j<n;j++){
            cin>>lewe[i][j];
            lewe[i][j]+=lewe[i][j-1];
        }
	}
	for(int i=0;i<p;i++){
            cin>>prawe[i][0];
        for(int j=1;j<n;j++){
            cin>>prawe[i][j];
            prawe[i][j]+=prawe[i][j-1];
        }
	}
	emptyarray(DPL1);
	DPL1[ind(0)]=0;
	emptyarray(DPP1);
	DPP1[ind(0)]=0;
    //COUNT LEFT
	for(int i=0;i<l;i++){
            copyArray(DPL1,DPL2);
        for(int j=0;j<n;j++){
            int mass=-MAXM;
            int stop=MAXM;
            if(lewe[i][j]>=0)
                mass+=lewe[i][j];
            else
                stop+=lewe[i][j];
           for(;mass<=stop;mass++){
            if (DPL1[ind(mass)]>=0){
                    DPL2[ind(mass + lewe[i][j])] = max(DPL1[ind(mass)]+j+1,DPL1[ind(mass+lewe[i][j])]);
			}
           }
        }
        copyArray(DPL2,DPL1);
	}
	//COUNT RIGHT
	for(int i=0;i<p;i++){
            copyArray(DPP1,DPP2);
        for(int j=0;j<n;j++){
            int mass=-MAXM;
            int stop=MAXM;
            if(prawe[i][j]>=0)
                mass+=prawe[i][j];
            else
                stop+=prawe[i][j];
           for(;mass<=stop;mass++){
            if (DPP1[ind(mass)]>=0){
                if(DPP1[ind(mass+prawe[i][j])]>=0)
                    DPP2[ind(mass + prawe[i][j])] = max(DPP1[ind(mass)]+j+1,DPP1[ind(mass+prawe[i][j])]);
                else
                    DPP2[ind(mass + prawe[i][j])]=DPP1[ind(mass)]+j+1;
			}
           }
        }
        copyArray(DPP2,DPP1);
	}
	int wynik = -1;
	//WYNIK
	for(int i=0;i<2*MAXM+1;i++){
        if(DPL1[i]>=0&&DPP1[i]>=0)
          wynik=max(wynik,DPL1[i]+DPP1[i]);
	}
	cout<<(l*n)+(p*n)-wynik;
}

