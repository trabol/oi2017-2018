// Wyszukiwanie cykli i �cie�ek Hamiltona
// Data: 10.02.2014
// (C)2014 mgr Jerzy Wa�aszek
//---------------------------------------

#include <iostream>
#include <bits/stdc++.h>

using namespace std;

struct Wezel{
    int klucz;
    list<Wezel*> dzieci;
    list<Wezel*> rodzice;
};
int n,k,wynik=0;
Wezel wezelki[1000001];

void sprawdzWezel(Wezel* wezel,int level){
    if(wezel->dzieci.size()==0){
        wynik+=min(k/2,level);
        return;
    }else{
        for (auto const& i : wezel->dzieci) {
            i->dzieci.remove(wezel);
            i->rodzice.remove(wezel);
            for (auto const& j : i->dzieci) {
                if(wezel->klucz==j->klucz){i->dzieci.remove(j);break;}
            }
            for (auto const& j : i->rodzice) {
                if(wezel->klucz==j->klucz)continue;
                else i->dzieci.push_back(j);
            }
            for (auto const& j : i->dzieci) {
            }
            sprawdzWezel(i,level+1);
        }
    }
}


int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin>>n>>k;
    if(k==1){
        cout<<1<<endl<<1;
        return 0;
    }else if(k>=n){
        cout<<n<<endl;
        for(int i=1;i<=n;i++){
            cout<<i<<" ";
        }
        return 0;
    }
    for(int i=1;i<n;i++){
        int a,b;
        cin>>a>>b;
        wezelki[a].klucz=a;
        wezelki[b].klucz=b;
        wezelki[a].dzieci.push_back(&wezelki[b]);
        wezelki[b].rodzice.push_back(&wezelki[a]);
    }
    Wezel korzen;
    for(int i=1;i<=n;i++){
        if(wezelki[i].rodzice.size()==0){
            korzen=wezelki[i];
            break;
        }
    }
    sprawdzWezel(&korzen,0);wynik++;
    cout<<wynik<<endl;

}
