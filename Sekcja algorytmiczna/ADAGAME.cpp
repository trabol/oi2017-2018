#include <iostream>
#include <math.h>
int T;

int tests[200][2];

using namespace std;

void read(){
    cin>>T;
    for(int i=0;i<T;i++)
        cin>>tests[i][0]>>tests[i][1];
}

int main(){
    read();
    for(int i=0;i<T;i++){
        int N=tests[i][0];
        int M=tests[i][1];
        if(M==0){cout<<"Vinit"<<endl;continue;}
        static bool lastTab[10000][4];
        int k=0;
        if(M%2==1){//Ada konczy
         for(int j=0;j<10000;j++){

            int a=j/1000;
            int b=j/100-10*a;
            int c=j/10 - 100*a - 10*b;
            int d=j%10;
            lastTab[j][0]=(j<1000*((a+1)%10)+100*b+10*c+d);
            lastTab[j][1]=(j<1000*a+100*((b+1)%10)+10*c+d);
            lastTab[j][2]=(j<1000*a+100*b+10*((c+1)%10)+d);
            lastTab[j][3]=(j<1000*a+100*b+10*c+((d+1)%10));
         }
        }else{
           for(int j=0;j<10000;j++){//Vincent konczy
            k+=1;
            int a=j/1000;
            int b=j/100-10*a;
            int c=j/10 - 100*a - 10*b;
            int d=j%10;
            int a2=a-1;
            int b2=b-1;
            int c2=c-1;
            int d2=d-1;
            if(a2<0)a2=10+a2;
            if(b2<0)b2=10+b2;
            if(c2<0)c2=10+c2;
            if(d2<0)d2=10+d2;
            lastTab[j][0]=!(j<1000*((a2)%10)+100*b+10*c+d);
            lastTab[j][1]=!(j<1000*a+100*((b2)%10)+10*c+d);
            lastTab[j][2]=!(j<1000*a+100*b+10*((c2)%10)+d);
            lastTab[j][3]=!(j<1000*a+100*b+10*c+((d2)%10));
         }
        }
        for(;k<M-1;k++){
            static bool newTab[10000][4];
            for(int j=0;j<10000;j++){
                if(k%2==0){//Vincent
                    int a=j/1000;
                    int b=j/100-10*a;
                    int c=j/10 - 100*a - 10*b;
                    int d=j%10;
                    int num0=1000*((a+1)%10)+100*b+10*c+d;
                    newTab[j][0]=lastTab[num0][0]&&lastTab[num0][1]&&lastTab[num0][2]&&lastTab[num0][3];
                    int num1=1000*a+100*((b+1)%10)+10*c+d;
                    newTab[j][0]=lastTab[num1][0]&&lastTab[num1][1]&&lastTab[num1][2]&&lastTab[num1][3];
                    int num2=j<1000*a+100*b+10*((c+1)%10)+d;
                    newTab[j][0]=lastTab[num2][0]&&lastTab[num2][1]&&lastTab[num2][2]&&lastTab[num2][3];
                    int num3=1000*a+100*b+10*c+((d+1)%10);
                    newTab[j][0]=lastTab[num3][0]&&lastTab[num3][1]&&lastTab[num3][2]&&lastTab[num3][3];

                }else{//Ada
                    int a=j/1000;
                    int b=j/100-10*a;
                    int c=j/10 - 100*a - 10*b;
                    int d=j%10;
                    int num0=1000*((a+1)%10)+100*b+10*c+d;
                    newTab[j][0]=lastTab[num0][0]||lastTab[num0][1]||lastTab[num0][2]||lastTab[num0][3];
                    int num1=1000*a+100*((b+1)%10)+10*c+d;
                    newTab[j][0]=lastTab[num1][0]||lastTab[num1][1]||lastTab[num1][2]||lastTab[num1][3];
                    int num2=j<1000*a+100*b+10*((c+1)%10)+d;
                    newTab[j][0]=lastTab[num2][0]||lastTab[num2][1]||lastTab[num2][2]||lastTab[num2][3];
                    int num3=1000*a+100*b+10*c+((d+1)%10);
                    newTab[j][0]=lastTab[num3][0]||lastTab[num3][1]||lastTab[num3][2]||lastTab[num3][3];
                }
            }
            for(int a=0;a<10000;a++){
                for(int b=0;b<4;b++){
                    lastTab[a][b]=newTab[a][b];
                }
            }
        }
        if(lastTab[tests[i][0]][0]||lastTab[tests[i][0]][1]||lastTab[tests[i][0]][2]||lastTab[tests[i][0]][3])
            cout<<"Ada"<<endl;
        else
            cout<<"Vinit"<<endl;
    }
}
